//
//  LoginViewModel_Test.swift
//  EarthQuakesAppTests
//
//  Created by Divier on 15/11/2023.
//

import XCTest
import Combine
@testable import EarthQuakesApp

final class LoginViewModel_Test: XCTestCase {
    
    private var cancelable: Set<AnyCancellable> = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        cancelable.removeAll()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_LoginViewModel_validateEmailAndPass_success() {
        //Given
        let vm = LoginViewModel()
        
        //When
        let email = "test@g.com"
        let password = "test"
        vm.login(email: email, password: password)
        
        //Them
        XCTAssertFalse(vm.showErrorAlert, "No error message")
        XCTAssertTrue(vm.showErrorMessage.isEmpty)
        XCTAssertTrue(vm.showErrorMessage == "")
    }
    
    func test_LoginViewModel_validateEmailAndPass_fail() {
        //Given
        let vm = LoginViewModel()
        
        //When
        let email = "testcom"
        let password = "test"
        vm.login(email: email, password: password)
        
        //Them
        XCTAssertTrue(vm.showErrorAlert, "Should show error message")
        XCTAssertFalse(vm.showErrorMessage.isEmpty)
        XCTAssertFalse(vm.showErrorMessage == "")
    }
    
    func test_LoginViewModel_Pass_fail() {
        //Given
        let vm = LoginViewModel()
        
        //When
        let email = "test@g.com"
        let password = ""
        vm.login(email: email, password: password)
        
        //Them
        XCTAssertTrue(vm.showErrorAlert, "Should show error message")
        XCTAssertFalse(vm.showErrorMessage.isEmpty)
        XCTAssertFalse(vm.showErrorMessage == "")
    }
    
    func test_LoginViewModel_login_success() {
        //Given
        UserDataServices.shared.saveUser(name: "test",
                                         surname: "test",
                                         email: "Test1@gmail.com",
                                         password: "Test")
        let vm = LoginViewModel()
        
        //When
        let email = "Test1@gmail.com"
        let password = "Test"
        vm.login(email: email, password: password)
        let expectation = XCTestExpectation(description: "Should return user logged")
        
        vm.$userLogged.sink { item in
            expectation.fulfill()
        }.store(in: &cancelable)
        
        //Them
        wait(for: [expectation], timeout: 1)
        XCTAssertNotNil(vm.userLogged)
        XCTAssertFalse(vm.showErrorAlert, "No error message")
        XCTAssertTrue(vm.showErrorMessage.isEmpty)
        
    }
    
    func test_LoginViewModel_logout_success() {
        //Given
        UserDataServices.shared.saveUser(name: "test",
                                         surname: "test",
                                         email: "Test1@gmail.com",
                                         password: "Test")
        let vm = LoginViewModel()
        
        //When
        let email = "Test1@gmail.com"
        let password = "Test"
        vm.login(email: email, password: password)
        let expectation = XCTestExpectation(description: "Should return user logged")
        
        vm.$userLogged.sink { item in
            expectation.fulfill()
        }.store(in: &cancelable)
        
        wait(for: [expectation], timeout: 1)
        vm.logout()
        //Them
        
        XCTAssertNil(vm.userLogged)
        
    }
    
}
