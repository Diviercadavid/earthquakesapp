//
//  DetailViewModel_Test.swift
//  EarthQuakesAppTests
//
//  Created by Divier on 15/11/2023.
//

import XCTest
import Combine
@testable import EarthQuakesApp

final class DetailViewModel_Test: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_DetailViewModel_loadCoordinates_success() {
        let vm = DetailViewModel(record: DeveloperPreview.instance.record1)
        
        XCTAssertNotNil(vm.latitude)
        XCTAssertNotNil(vm.longitude)
        XCTAssertNotNil(vm.record)
    }

    func test_DetailViewModel_loadCoordinates_Zero() {
        let vm = DetailViewModel(record: RecordModel(properties: nil, geometry: nil))
        
        XCTAssertNotNil(vm.latitude)
        XCTAssertNotNil(vm.longitude)
        XCTAssertNotNil(vm.record)
        XCTAssertEqual(vm.latitude, 0.0)
        XCTAssertEqual(vm.longitude, 0.0)
    }
}
