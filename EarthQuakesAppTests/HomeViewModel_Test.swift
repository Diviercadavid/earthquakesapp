//
//  HomeViewModel_Test.swift
//  EarthQuakesAppTests
//
//  Created by Divier on 15/11/2023.
//

import XCTest
import Combine
@testable import EarthQuakesApp

final class HomeViewModel_Test: XCTestCase {

    private var cancelable: Set<AnyCancellable> = Set<AnyCancellable>()
    let formatter = DateFormatter()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        cancelable.removeAll()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_HomeViewModel_setDates_fail() {
        
        let vm = HomeViewModel()
        sleep(3) //wait for first request 
        formatter.dateFormat = "yyyy-MM-dd"
        
        let startDate = formatter.date(from: "2016-10-08")
        let endDate = formatter.date(from: "2016-10-05")
        let expectation = expectation(description: "Dates Error")
        
        vm.searchRecords(startDate: startDate!, endDate: endDate!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssertTrue(vm.showErrorAlert)
            XCTAssertFalse(vm.showErrorAlertMessage.isEmpty)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 2)
       
    }

    func test_HomeViewModel_setDates_success() {
        
        let vm = HomeViewModel()
        sleep(3) //wait for first request
        formatter.dateFormat = "yyyy-MM-dd"
        
        let startDate = formatter.date(from: "2016-10-05")
        let endDate = formatter.date(from: "2016-10-08")
        let expectation = expectation(description: "Dates success")
        
        vm.searchRecords(startDate: startDate!, endDate: endDate!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            XCTAssertFalse(vm.showErrorAlert)
            XCTAssertTrue(vm.showErrorAlertMessage.isEmpty)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 2)
    }
    
    func test_HomeViewModel_returnData_success() {
        
        let vm = HomeViewModel()
        sleep(3) //wait for first request
        formatter.dateFormat = "yyyy-MM-dd"
        
        let startDate = formatter.date(from: "2016-10-05")
        let endDate = formatter.date(from: "2016-10-08")
        let expectation = expectation(description: "waiting any response")
        
        vm.searchRecords(startDate: startDate!, endDate: endDate!)
        vm.$recordsToShow
            .dropFirst()
            .sink { items in
            expectation.fulfill()
        }
        .store(in: &cancelable)
        
        wait(for: [expectation], timeout: 3)
        XCTAssertNotEqual(vm.recordsToShow.count, 0)
    }
    
    func test_HomeViewModel_loadNextPage_success() {
        
        let vm = HomeViewModel()
        sleep(3) //wait for first request

        XCTAssertNotEqual(vm.recordsToShow.count, 10)
        
        vm.loadNextPage()
        
        XCTAssertNotEqual(vm.recordsToShow.count, 20)
    }
}
