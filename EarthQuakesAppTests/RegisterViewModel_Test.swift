//
//  RegisterViewModel_Test.swift
//  EarthQuakesAppTests
//
//  Created by Divier on 15/11/2023.
//

import XCTest
import Combine
@testable import EarthQuakesApp
final class RegisterViewModel_Test: XCTestCase {

    private var cancelable: Set<AnyCancellable> = Set<AnyCancellable>()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        cancelable.removeAll()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_RegisterViewModel_validateEmail_fail() {
        let vm = RegisterViewModel()
        
        vm.saveUser(name: "test", surname: "test", email: "test", password: "")
        
        XCTAssertTrue(vm.showAlert, "should show an alert")
        XCTAssertFalse(vm.alertMessage.isEmpty)
    }

    func test_RegisterViewModel_validateEmail_success() {
        let vm = RegisterViewModel()
        
        vm.saveUser(name: "test", surname: "test", email: "\(UUID().uuidString)Test@g.com", password: "test")
        
        sleep(2)
        XCTAssertTrue(vm.showAlert, "should show an alert")
        XCTAssertEqual(vm.alertMessage, "user saved successfully")
    }
}
