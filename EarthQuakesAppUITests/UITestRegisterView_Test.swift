//
//  UITestRegisterView_Test.swift
//  EarthQuakesAppUITests
//
//  Created by Divier on 15/11/2023.
//

import XCTest

final class UITestRegisterView_Test: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUpWithError() throws {
        
        continueAfterFailure = false

        app.launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class
    }

    func test_RegisterView_createUser_success() throws {
        
        XCUIApplication().scrollViews.otherElements.buttons["registerButton"].tap()
        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery/*@START_MENU_TOKEN@*/.textFields["Name"]/*[[".cells.textFields[\"Name\"]",".textFields[\"Name\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        collectionViewsQuery/*@START_MENU_TOKEN@*/.textFields["Surname"]/*[[".cells.textFields[\"Surname\"]",".textFields[\"Surname\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        collectionViewsQuery/*@START_MENU_TOKEN@*/.textFields["Email"]/*[[".cells.textFields[\"Email\"]",".textFields[\"Email\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields(writeEmail: true)
        
        collectionViewsQuery/*@START_MENU_TOKEN@*/.textFields["Password"]/*[[".cells.textFields[\"Password\"]",".textFields[\"Password\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        app.buttons["saveButton"].tap()
        
        sleep(2)
        
        
        let alertShowing = app.alerts.element(boundBy: 0)
        
        XCTAssertTrue(alertShowing.exists, "should show an alert")
    }
    
    func test_RegisterView_emptyFields_fail() {
        
        XCUIApplication().scrollViews.otherElements.buttons["registerButton"].tap()
        
        app.buttons["saveButton"].tap()
        
        let alertShowing = app.alerts["Sorry: All fields should be fill properly"]
        
        sleep(1)
        
        XCTAssertTrue(alertShowing.exists, "should show an alert")
    }
    
    func test_RegisterView_createUserWithSameEmail_error() throws {
        
        XCUIApplication().scrollViews.otherElements.buttons["registerButton"].tap()

        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery/*@START_MENU_TOKEN@*/.textFields["Name"]/*[[".cells.textFields[\"Name\"]",".textFields[\"Name\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        collectionViewsQuery/*@START_MENU_TOKEN@*/.textFields["Surname"]/*[[".cells.textFields[\"Surname\"]",".textFields[\"Surname\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        collectionViewsQuery/*@START_MENU_TOKEN@*/.textFields["Email"]/*[[".cells.textFields[\"Email\"]",".textFields[\"Email\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields(writeEmail: true)
        
        collectionViewsQuery/*@START_MENU_TOKEN@*/.textFields["Password"]/*[[".cells.textFields[\"Password\"]",".textFields[\"Password\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        app.buttons["saveButton"].tap()
        
        sleep(2)
        
        let alertShowing = app.alerts["Error Saving data"]
        
        XCTAssertTrue(alertShowing.exists, "should show an alert")
        
    }
    
    
    private func writeRegisterFields(writeEmail: Bool = false) {
        let tKey = app/*@START_MENU_TOKEN@*/.keys["T"]/*[[".keyboards.keys[\"T\"]",".keys[\"T\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        tKey.tap()
        
        let eKey = app/*@START_MENU_TOKEN@*/.keys["e"]/*[[".keyboards.keys[\"e\"]",".keys[\"e\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        eKey.tap()
        
        let sKey = app/*@START_MENU_TOKEN@*/.keys["s"]/*[[".keyboards.keys[\"s\"]",".keys[\"s\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        sKey.tap()
        
        let tKey2 = app/*@START_MENU_TOKEN@*/.keys["t"]/*[[".keyboards.keys[\"t\"]",".keys[\"t\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        tKey2.tap()
        
        if writeEmail {
            let key = app/*@START_MENU_TOKEN@*/.keys["@"]/*[[".keyboards.keys[\"@\"]",".keys[\"@\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            key.tap()
            
            let gKey = app/*@START_MENU_TOKEN@*/.keys["g"]/*[[".keyboards.keys[\"g\"]",".keys[\"g\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            gKey.tap()
            
            let mKey = app/*@START_MENU_TOKEN@*/.keys["m"]/*[[".keyboards.keys[\"m\"]",".keys[\"m\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            mKey.tap()
            
            let key2 = app/*@START_MENU_TOKEN@*/.keys["."]/*[[".keyboards.keys[\".\"]",".keys[\".\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            key2.tap()
            
            let cKey = app/*@START_MENU_TOKEN@*/.keys["c"]/*[[".keyboards.keys[\"c\"]",".keys[\"c\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            cKey.tap()
            
            let oKey = app/*@START_MENU_TOKEN@*/.keys["o"]/*[[".keyboards.keys[\"o\"]",".keys[\"o\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            oKey.tap()
            mKey.tap()
        }
        
        app/*@START_MENU_TOKEN@*/.buttons["Return"]/*[[".keyboards",".buttons[\"return\"]",".buttons[\"Return\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
    }

    
}
