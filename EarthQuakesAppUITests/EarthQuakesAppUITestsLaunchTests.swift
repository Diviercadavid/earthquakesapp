//
//  EarthQuakesAppUITestsLaunchTests.swift
//  EarthQuakesAppUITests
//
//  Created by Divier on 14/11/2023.
//

import XCTest

final class EarthQuakesAppUITestsLaunchTests: XCTestCase {

    override class var runsForEachTargetApplicationUIConfiguration: Bool {
        true
    }

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testLaunch() throws {
        
    }
}
