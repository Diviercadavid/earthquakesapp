//
//  UITstDetailView_Test.swift
//  EarthQuakesAppUITests
//
//  Created by Divier on 15/11/2023.
//

import XCTest

final class UITestDetailView_Test: XCTestCase {

    let app = XCUIApplication()
    
    override func setUpWithError() throws {
        
        app.launch()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_HomeView_selectDates_success() {
        
        homeView_login_success()
        
//        let app = XCUIApplication()
        
        let listitemsCollectionView = app.collectionViews["listItems"]
        
        sleep(5)
        
        XCTAssertTrue(listitemsCollectionView.exists)
        
        let firstItem = listitemsCollectionView.children(matching: .cell).element(boundBy: 1).children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element
        
        firstItem.tap()
        
        let scrollViewsQuery = app.scrollViews
        
        let map = scrollViewsQuery.otherElements.containing(.staticText, identifier:"M 2.3 - 4 km WNW of Ponce, Puerto Rico").children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .map).element
        map.tap()
        
        sleep(3)
        
        XCTAssertTrue(map.exists)
                
    }

    func homeView_login_success() {
        XCUIApplication().scrollViews.otherElements/*@START_MENU_TOKEN@*/.textFields["email"]/*[[".textFields[\"Email\"]",".textFields[\"email\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields(writeEmail: true)
        
        XCUIApplication().scrollViews.otherElements/*@START_MENU_TOKEN@*/.secureTextFields["password"]/*[[".secureTextFields[\"Password\"]",".secureTextFields[\"password\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.buttons["shift"]/*[[".keyboards.buttons[\"shift\"]",".buttons[\"shift\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        XCUIApplication().scrollViews.otherElements.buttons["loginButton"].tap()
        
        let alertShowing = app.alerts["Something was wrong"]
        
        sleep(1)
        
        XCTAssertFalse(alertShowing.exists, "should show an alert")
      
    }
    
    private func writeRegisterFields(writeEmail: Bool = false) {
        let uKey = app.keys["U"] // email: "Usr@gm.com", password: "Usr")
        uKey.tap()
        
        let sKey = app.keys["s"]
        sKey.tap()
        
        let rKey = app.keys["r"]
        rKey.tap()
        
        if writeEmail {
            let key = app/*@START_MENU_TOKEN@*/.keys["@"]/*[[".keyboards.keys[\"@\"]",".keys[\"@\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            key.tap()
            
            let gKey = app/*@START_MENU_TOKEN@*/.keys["g"]/*[[".keyboards.keys[\"g\"]",".keys[\"g\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            gKey.tap()
            
            let mKey = app/*@START_MENU_TOKEN@*/.keys["m"]/*[[".keyboards.keys[\"m\"]",".keys[\"m\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            mKey.tap()
            
            let key2 = app/*@START_MENU_TOKEN@*/.keys["."]/*[[".keyboards.keys[\".\"]",".keys[\".\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            key2.tap()
            
            let cKey = app/*@START_MENU_TOKEN@*/.keys["c"]/*[[".keyboards.keys[\"c\"]",".keys[\"c\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            cKey.tap()
            
            let oKey = app/*@START_MENU_TOKEN@*/.keys["o"]/*[[".keyboards.keys[\"o\"]",".keys[\"o\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            oKey.tap()
            mKey.tap()
        }
        
        app/*@START_MENU_TOKEN@*/.buttons["Return"]/*[[".keyboards",".buttons[\"return\"]",".buttons[\"Return\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
    }
}
