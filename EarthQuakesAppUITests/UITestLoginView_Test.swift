//
//  UITestLoginView_Test.swift
//  EarthQuakesAppUITests
//
//  Created by Divier on 15/11/2023.
//

import XCTest

final class UITestLoginView_Test: XCTestCase {

    let app = XCUIApplication()
    
    override func setUpWithError() throws {
        
        continueAfterFailure = false

        app.launch()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class
    }
    
    
    func test_LoginView_credentials_fail() {
        XCUIApplication().scrollViews.otherElements.buttons["loginButton"].tap()
        
        let alertShowing = app.alerts["Something was wrong"]
        
        sleep(1)
        
        XCTAssertTrue(alertShowing.exists, "should show an alert")
    }
    
    func test_LoginView_wrongEmail_fail() {
        XCUIApplication().scrollViews.otherElements/*@START_MENU_TOKEN@*/.textFields["email"]/*[[".textFields[\"Email\"]",".textFields[\"email\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        XCUIApplication().scrollViews.otherElements.buttons["loginButton"].tap()
        
        let alertShowing = app.alerts["Something was wrong"]
        
        sleep(1)
        
        XCTAssertTrue(alertShowing.exists, "should show an alert")
    }
    
    func test_LoginView_login_success() {
        XCUIApplication().scrollViews.otherElements/*@START_MENU_TOKEN@*/.textFields["email"]/*[[".textFields[\"Email\"]",".textFields[\"email\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields(writeEmail: true)
        
        XCUIApplication().scrollViews.otherElements/*@START_MENU_TOKEN@*/.secureTextFields["password"]/*[[".secureTextFields[\"Password\"]",".secureTextFields[\"password\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app/*@START_MENU_TOKEN@*/.buttons["shift"]/*[[".keyboards.buttons[\"shift\"]",".buttons[\"shift\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        writeRegisterFields()
        
        XCUIApplication().scrollViews.otherElements.buttons["loginButton"].tap()
        
        let alertShowing = app.alerts["Something was wrong"]
        
        sleep(1)
        
        XCTAssertFalse(alertShowing.exists, "should show an alert")
      
    }
    
    private func writeRegisterFields(writeEmail: Bool = false) {
        let uKey = app.keys["U"] // email: "Usr@gm.com", password: "Usr")
        uKey.tap()
        
        let sKey = app.keys["s"]
        sKey.tap()
        
        let rKey = app.keys["r"]
        rKey.tap()
        
        if writeEmail {
            let key = app/*@START_MENU_TOKEN@*/.keys["@"]/*[[".keyboards.keys[\"@\"]",".keys[\"@\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            key.tap()
            
            let gKey = app/*@START_MENU_TOKEN@*/.keys["g"]/*[[".keyboards.keys[\"g\"]",".keys[\"g\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            gKey.tap()
            
            let mKey = app/*@START_MENU_TOKEN@*/.keys["m"]/*[[".keyboards.keys[\"m\"]",".keys[\"m\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            mKey.tap()
            
            let key2 = app/*@START_MENU_TOKEN@*/.keys["."]/*[[".keyboards.keys[\".\"]",".keys[\".\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            key2.tap()
            
            let cKey = app/*@START_MENU_TOKEN@*/.keys["c"]/*[[".keyboards.keys[\"c\"]",".keys[\"c\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            cKey.tap()
            
            let oKey = app/*@START_MENU_TOKEN@*/.keys["o"]/*[[".keyboards.keys[\"o\"]",".keys[\"o\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
            oKey.tap()
            mKey.tap()
        }
        
        app/*@START_MENU_TOKEN@*/.buttons["Return"]/*[[".keyboards",".buttons[\"return\"]",".buttons[\"Return\"]"],[[[-1,2],[-1,1],[-1,0,1]],[[-1,2],[-1,1]]],[0]]@END_MENU_TOKEN@*/.tap()
    }
}
