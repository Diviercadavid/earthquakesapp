<h1 align="center">
  <br>
  <a href="http://www.amitmerchant.com/electron-markdownify"><img src="https://gitlab.com/Diviercadavid/earthquakesapp/-/raw/main/src/radar.png?ref_type=heads" alt="Markdownify" width="200"></a>
  <br>
  EarthQuakes app
  <br>
</h1>

<h4 align="center">Simple App shows record about earthquakes filtering by dates showing details on the map.</h4>

<p align="center">
  <a href="https://badge.fury.io/js/electron-markdownify">
    <img src="https://img.shields.io/badge/Swift-FA7343?style=for-the-badge&logo=swift&logoColor=white"
         alt="Gitter">
  </a>
</p>

<p align="center">
  <a href="#key-features">Persistence</a> •
  <a href="#how-to-use">SwiftUI</a> •
  <a href="#download">URLSession</a> •
  <a href="#mvvm">MVVM</a> •
  <a href="#coredata">CoreData</a> •
  <a href="#maps">Maps</a> •
  <a href="#license">License</a>
</p>


  <a href="http://www.amitmerchant.com/electron-markdownify"><img src="https://gitlab.com/Diviercadavid/earthquakesapp/-/raw/main/src/snapshot.gif?ref_type=heads" alt="Markdownify" width="400"></a>
  
  <a href="http://www.amitmerchant.com/electron-markdownify"><img src="https://gitlab.com/Diviercadavid/earthquakesapp/-/raw/main/src/snapshot1.gif?ref_type=heads" alt="Markdownify" width="400"></a>
    

## About EarthQuakes app
 EarthQuakes app was made completely with SwiftUI and Swift 5, Architecture pattern implemented was MVVM and some design pattern such as Observer and Singleton. Data is saved in Core Data and services are working with URLSession and Combine.
* Register a new User and they can make login 
* Allow manage users and loginservice and save in local
  - Saving data in CoraData
* Show earthquakes records by a range o dates 
* Show details about a record selected in a Map
* Comunication with USGS.gov using URLSession
* Made with MVVM arquitecture
* Useing SwiftUI and Navigationstack
#


## Credits

This software uses the following open source packages:

![screenshot](https://www.usgs.gov/themes/custom/usgs_tantalum/usgs_logo.png)

<a href="http://www.amitmerchant.com/electron-markdownify"><img src="https://media.flaticon.com/dist/min/img/logo/flaticon_negative.svg" alt="Markdownify" width="300"></a>

- [USGS API](https://www.usgs.gov)

- <a href="https://www.flaticon.es/iconos-gratis/deteccion" title="detección iconos">Icon detection created by Freepik - Flaticon </a>


- [Developed by Divier Cadavid ](https://www.linkedin.com/in/diviercadavid/)


## Thanks for watching


## 

