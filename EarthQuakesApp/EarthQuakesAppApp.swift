//
//  EarthQuakesAppApp.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//
//✅ 1. create project
//✅ 2. set packages
//✅ 3. create models (request response, user)
//✅ 4. create store (CoreData)
//✅ 5. create workflow to register, login, logout
//✅ 6. create request services
//✅ 7. crate pagination
//✅ 8. create view with filter
//✅ 9. create detailView (with map)
//✅ 10. check details (error handlers, nice views, validations)
// 11. UnitTest
// 11. UITest


import SwiftUI

@main
struct EarthQuakesAppApp: App {
    
    @StateObject private var vm: LoginViewModel = LoginViewModel()
    
    var body: some Scene {
        WindowGroup {
            LoginView(navManager: NavigationManager())
                .environmentObject(vm)
                .preferredColorScheme(.light)
        }
        
    }
}
