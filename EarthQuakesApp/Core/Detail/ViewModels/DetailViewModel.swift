//
//  DetailViewModel.swift
//  EarthQuakesApp
//
//  Created by Divier on 15/11/2023.
//

import Foundation

class DetailViewModel: ObservableObject {
    
    @Published var record: RecordModel?
    
    var latitude: Double {
        get { return record?.geometry?.coordinates?[1] ?? 0.0 }
    }
    var longitude: Double {
        get { return record?.geometry?.coordinates?[0] ?? 0.0}
    }
    
    init(record: RecordModel?) {
        self.record = record
    }
    
}
