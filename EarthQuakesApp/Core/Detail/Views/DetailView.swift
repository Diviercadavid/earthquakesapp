//
//  DetailView.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//
import MapKit
import SwiftUI

struct DetailView: View {
    @StateObject var detailViewModel: DetailViewModel
    
    init(record: RecordModel) {
        _detailViewModel = StateObject(wrappedValue: .init(record: record))
        print("detailView init()")
    }
    
    var body: some View {
        ScrollView {
            VStack {
                
                title
                
                mapView
                
                info
                
                Spacer()
            }
            .frame(maxWidth: .infinity)
            .ignoresSafeArea(.all)
        }
    }
}
extension DetailView {
    
    private var title: some View {
        Text("\(detailViewModel.record?.properties?.title ?? "")")
            .font(.system(size: 28))
            .frame(maxWidth: .infinity,alignment: .leading)
            .bold()
            .lineLimit(2)
            .padding(.horizontal, 16)
            .foregroundColor(.titleTextApp)
    }
    
    private var mapView: some View {
        ZStack {
            Map(position: .constant(MapCameraPosition.region(getRegion())) ) {
                Marker("Epicentre", coordinate: CLLocationCoordinate2D(latitude: detailViewModel.latitude,
                                                                    longitude: detailViewModel.longitude))
                
                
            }
            .frame(height: 400)
            
            
            Rectangle()
                .foregroundColor(.clear)
                .background(LinearGradient(gradient: Gradient(colors: [.clear, .clear, .white]), startPoint: .top, endPoint: .bottom))
                
        }}
    
    private var info: some View {
        VStack {
            LocationLabelView(place: detailViewModel.record?.properties?.place ?? "")
                .frame(maxWidth: .infinity, alignment: .leading)
                .foregroundColor(.accentColorApp)
                .padding(.top, -70)
                .padding()
            
            HStack {
                Text("\(detailViewModel.record?.properties?.depth?.formatter2Digits ?? "0.0") rms")
                    .font(.system(size:18))
                
                Text("\(detailViewModel.record?.properties?.mag?.formatter2Digits ?? "0.0") mag")
                    .font(.system(size:18))
                    
                Spacer()
                
            }
            .foregroundStyle(Color.contentTextColorApp)
            .padding(.top, -70)
            .padding()
        }
        
    }
    
    private func getRegion() -> MKCoordinateRegion {
        MKCoordinateRegion(
            center: CLLocationCoordinate2D(latitude: detailViewModel.latitude, longitude: detailViewModel.longitude),
            span: MKCoordinateSpan(latitudeDelta: 0.09, longitudeDelta: 0.05)
        )
    }
    
}

#Preview {
    DetailView(record: DeveloperPreview.instance.record3)
}
