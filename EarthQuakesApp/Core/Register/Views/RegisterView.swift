//
//  RegisterView.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import SwiftUI

struct RegisterView: View {
    
    @State var name: String = ""
    @State var surname: String = ""
    @State var email: String = ""
    @State var password: String = ""
    
    
    @StateObject var registerViewModel = RegisterViewModel()
    
    var body: some View {
        NavigationStack {
            fields
                .navigationTitle(Text("Register"))
                .navigationBarTitleDisplayMode(.large)
                .onTapGesture {
                    UIApplication.shared.endEditing()
                }
                .alert(registerViewModel.alertMessage, isPresented: $registerViewModel.showAlert) {
                    Button("I got it") {
                        registerViewModel.showAlert.toggle()
                    }
                    .accessibilityIdentifier("dismissAlert")
                }
            
            saveButton
        }
        
        
    }
}
extension RegisterView {
    
    private var saveButton: some View {
        Button(action: {
            registerViewModel.saveUser(name: name, surname: surname, email: email, password: password)
            
        }, label: {
            Text("Save")
                .frame(maxWidth: .infinity)
                .foregroundColor(.primaryColorApp)
                .padding()
        })
        .accessibilityIdentifier("saveButton")
    }
    
    private var fields: some View {
        Form {
            TextField("Name",text: $name)
                .foregroundStyle(Color.contentTextColorApp)
            TextField("Surname",text: $surname)
                .foregroundStyle(Color.contentTextColorApp)
            TextField("Email",text: $email)
                .foregroundStyle(Color.contentTextColorApp)
                .keyboardType(.emailAddress)
            TextField("Password",text: $password)
                .foregroundStyle(Color.contentTextColorApp)
        }
    }
}

#Preview {
    RegisterView()
}
