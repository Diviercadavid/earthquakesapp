//
//  RegisterViewModel.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation
import Combine

class RegisterViewModel: ObservableObject {
    
    private var userDataService = UserDataServices.shared
    private var cancelables = Set<AnyCancellable>()
    @Published var showAlert: Bool = false
    @Published var alertMessage: String = ""
    
    init() {
        userDataService.$transactionResult
            .dropFirst()
            .sink { [weak self] result in
            self?.validateUserServiceResult(result: result)
        }
        .store(in: &cancelables)
    }
    
    func saveUser(name: String, surname: String, email: String, password: String) {
        
        guard !name.isEmpty, !surname.isEmpty, !email.isEmpty, email.isValidEmail(), !password.isEmpty
        else {
            showAlert(message: "Sorry: All fields should be fill properly")
            return
        }
        
        userDataService.saveUser(name: name, surname: surname, email: email, password: password)
        
    }
    
    private func validateUserServiceResult(result: Result<String, CustomError>?) {
        if case .failure(let error) = result {
            showAlert(message: error.localizedDescription)
        }
        
        if case .success(let message) = result {
            showAlert(message: message)
        }
    }
    
    private func showAlert(message: String) {
        showAlert = true
        alertMessage = message
    }
}
