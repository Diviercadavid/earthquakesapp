//
//  LoginViewModel.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation
import Combine

class LoginViewModel: ObservableObject {
    
    @Published var showErrorAlert: Bool = false
    @Published var showErrorMessage: String = ""
    @Published var showHomeView : Bool = false
    @Published var userLogged: UserEntity?
    
    private var userDataService = UserDataServices.shared
    private var cancelables = Set<AnyCancellable>()
    
    init () {
        subscribeObservers()
    }
    

    func login(email: String, password: String) {
        
        guard email.isValidEmail() else {
            sendErrorMessage()
            return
        }
        
        guard !password.isEmpty else {
            sendErrorMessage()
            return
        }
        
        userDataService.getUser(email: email, password: password)
        
    }
    
    func logout() {
        userLogged = nil
    }
    
    
}

extension LoginViewModel {
    
    private func subscribeObservers() {
        userDataService
            .$userEntity
            .dropFirst()
            .sink { [weak self] userLogged in
                self?.showHomeView =  true
                self?.userLogged = userLogged
            }
            .store(in: &cancelables)
    }
    
    private func sendErrorMessage() {
        showErrorAlert = true
        showErrorMessage = "Something was wrong"
    }
}
