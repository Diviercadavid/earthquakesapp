//
//  LoginView.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import SwiftUI

struct LoginView: View {
    
    @State private var email =  "Usr@gm.com"
    @State private var password = "Usr"
    
    @State private var showRegisterView: Bool = false
    @State private var presentedViews: [RecordModel] = []

    @StateObject var navManager: NavigationManager
    
    @EnvironmentObject private var loginViewModel: LoginViewModel
    
    var body: some View {
        
        NavigationStack(path: $navManager.path) {
            ScrollView {
                VStack(spacing: 20) {
                    headerView
                    
                    fields
                    
                    buttons
                }
                .onChange(of: loginViewModel.userLogged, { _ , newValue in
                    if newValue != nil {
                        navManager.navigateTo(value: .home)
                    }
                })
                .padding()
            }
            .onTapGesture {
                UIApplication.shared.endEditing()
            }
            
            .navigationDestination(for: RecordModel.self) { item in
                DetailView(record: item)
            }
            .navigationDestination(for: NavigationType.self) { value in
                
                switch value {
                case .home:
                    HomeView(navManager: navManager).environmentObject(loginViewModel)
                case .about:
                    AboutView()
                }
            }
            .sheet(isPresented: $showRegisterView, content: {
                RegisterView()
            })
            .alert($loginViewModel.showErrorMessage.wrappedValue, isPresented: $loginViewModel.showErrorAlert, actions: {
                Button("Close") {
                    loginViewModel.showErrorAlert.toggle()
                }
            })
            
        }
        
    }
}

extension LoginView {
    
    private var headerView: some View {
        VStack {
            Image("Icon")
                .resizable()
                .frame(width: 100, height: 100)
            
            Text("EarthQuakesApp")
                .font(.system(size: 30))
                .foregroundStyle(Color.titleTextApp)
                .padding()
                .bold()
        }
    }
    
    private var fields: some View {
        VStack {
            TextField("Email", text: $email)
                .padding(.leading, 8)
                .frame(height: 50)
                .background(Color.secondaryColorApp.opacity(0.1))
                .foregroundStyle(Color.contentTextColorApp)
                .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                .keyboardType(.emailAddress)
                .accessibilityIdentifier("email")
                        
            SecureField("Password", text: $password)
                .padding(.leading, 8)
                .frame(height: 50)
                .background(Color.secondaryColorApp.opacity(0.1))
                .foregroundStyle(Color.contentTextColorApp)
                .autocorrectionDisabled(true)
                .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
                .accessibilityIdentifier("password")
        }
    }
    
    private var buttons: some View {
        VStack {
            
            Button(action: {
                loginViewModel.login(email: email, password: password)
                password = ""
            }, label: {
                Text("Login")
                    .foregroundStyle(Color.white)
                    .frame(maxWidth: .infinity)
                    .frame(height: 50)
                    .background(Color.primaryColorApp)
                    .clipShape(RoundedRectangle(cornerRadius: 10, style: .continuous))
            })
            .accessibilityIdentifier("loginButton")
            
            Button(action: {
                showRegisterView = true
            }, label: {
                Text("Register")
                    .frame(maxWidth: .infinity)
                    .frame(height: 50)
                    .foregroundStyle(Color.primaryColorApp)
            })
            .accessibilityIdentifier("registerButton")
        }
    }
}



#Preview {
    LoginView(navManager: NavigationManager())
        .environmentObject(LoginViewModel())
}
