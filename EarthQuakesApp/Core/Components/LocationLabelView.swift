//
//  LocationLabelView.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import SwiftUI

struct LocationLabelView: View {
    let place: String
    var body: some View {
        HStack {
            Image(systemName: "location")
                .font(.system(size: 16))
            Text(place)
                .font(.system(size:16))
                .lineLimit(1)
        }
        
    }
}

#Preview {
    LocationLabelView(place: "place")
}
