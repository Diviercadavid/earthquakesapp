//
//  AboutView.swift
//  EarthQuakesApp
//
//  Created by Divier on 21/11/2023.
//

import SwiftUI

struct AboutView: View {
    
    let linkedinProfile = URL (string: "https://www.linkedin.com/in/diviercadavid/")!
    let gitLabProfile = URL (string: "https://gitlab.com/Diviercadavid/profile-repository")!
    let servicesURL = URL (string: "https://www.usgs.gov")!
    let flaticonUrl = URL (string: "https://www.flaticon.es/iconos-gratis/deteccion")!
    
    
    var body: some View {
        List {
            aboutSection
            
            apiServices
            
            flatIcon
        }
        .navigationTitle("About EarthQuakes app")
        
    }
}

extension AboutView {
    
    private var aboutSection: some View {
        Section {
            VStack(alignment: .leading) {
                Image("Icon")
                    .resizable()
                    .frame(width: 100, height: 100)
                    .clipShape(RoundedRectangle(cornerRadius: 20))
                    .padding(.bottom, 16)
                
                Text("EarthQuakes app was made completely with SwiftUI and Swift 5, Architecture pattern implemented was MVVM and some design pattern such as Observer and Singleton. Data is saved in Core Data and services are working with URLSession and Combine.")
                    .font(.callout)
                    .fontWeight(.medium)
                    .foregroundStyle(Color.contentTextColorApp)
            }
            .padding(.vertical)
            
            Link("LinkedIn Profile", destination: linkedinProfile)
            Link("GirLab Profile", destination: gitLabProfile)
                
        } header: {
            Text("About")
                .foregroundStyle(Color.primaryColorApp)
        }
    }
    
    private var apiServices: some View {
        Section {
            VStack(alignment: .leading) {
                
                Text("Provide and apply earthquake science to reduce deaths, injuries, and property damage by understanding earthquake characteristics and effects, and offering information for mitigation.")
                    .font(.callout)
                    .fontWeight(.medium)
                    .foregroundStyle(Color.contentTextColorApp)
            }
            .padding(.vertical)
                
            Link("Visit USGS.gov", destination: servicesURL)
        } header: {
            Text("USGS services")
                .foregroundStyle(Color.primaryColorApp)
        }
    }
    
    private var flatIcon: some View {
        Section {
            VStack(alignment: .leading) {
                
                Text("They love helping individuals bring their ideas to life. Their desire is to grow every day, with the goal of providing the best high-quality content, including illustrations, photos, icons, mockups, and presentation templates.")
                    .font(.callout)
                    .fontWeight(.medium)
                    .foregroundStyle(Color.contentTextColorApp)
            }
            .padding(.vertical)
                
            Link("Visit flatIcon.com", destination: flaticonUrl)
        } header: {
            Text("flatIcon")
                .foregroundStyle(Color.primaryColorApp)
        }
    }
}

#Preview {
    AboutView()
}
