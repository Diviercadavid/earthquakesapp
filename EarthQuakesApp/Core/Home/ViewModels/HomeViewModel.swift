//
//  HomeViewModel.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation
import Combine

class HomeViewModel: ObservableObject {
    
    @Published var showErrorAlert: Bool = false
    @Published var showErrorAlertMessage: String = ""
    @Published var isLoading: Bool = false
    
    @Published var recordsToShow: [RecordModel] = []
    
    private var allRecords: [RecordModel] = []
    private var currentPage: Int = 1
    private let itemsPerPage: Int = 10
    
    private var earthDataService = EarthquakesDataService()
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        searchRecords()
        subscribeObservers()
    }
    
    private func subscribeObservers() {
        earthDataService
            .$recordsReturned
            .dropFirst()
            .sink { [weak self] items in
                self?.allRecords = items ?? []
                self?.updateDisplayedItems()
            }
            .store(in: &cancellables)
    }
    
    func searchRecords(startDate: Date? = Date().yesterday, 
                       endDate: Date? = Date.now) {
        
        guard let startDate = startDate , let endDate = endDate else { return }
        
        guard validateDates(startDate: startDate, endDate: endDate) else { return }
        isLoading = true
        recordsToShow.removeAll()
        earthDataService.getRecordsByDates(startTime: startDate.simpleFormatterDate(), endTime: endDate.simpleFormatterDate())
    }
}

extension HomeViewModel {
    
    private func validateDates(startDate: Date, endDate: Date) -> Bool {
        
        if startDate > endDate {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.showErrorAlert = true
                self.showErrorAlertMessage = "Dates selected are invalid"
                print("Dates invalid")
            }
            
            return false
        }
        
        return true
    }
    
    func loadNextPage() {
        let startIndex = currentPage * itemsPerPage
        let endIndex = min((currentPage + 1) * itemsPerPage, allRecords.count)
        if startIndex < endIndex {
            currentPage += 1
            updateDisplayedItems()
        }
    }
    
    private func updateDisplayedItems() {
        
        isLoading = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            
            guard let self = self else { return }
            let endIndex = min(self.currentPage * self.itemsPerPage, self.allRecords.count)
            self.isLoading = true
            self.recordsToShow = Array(self.allRecords.prefix(endIndex))
            
        }
        
    }
}
