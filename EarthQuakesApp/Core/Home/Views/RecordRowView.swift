//
//  RecordRowView.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import SwiftUI

struct RecordRowView: View {
    
    let record: RecordModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 8 ){
            Text(record.properties?.title ?? "")
                .font(.system(size:18))
                .lineLimit(1)
                .bold()
                .foregroundStyle(Color.titleTextApp)
            
            HStack {
                Text("\(record.properties?.depth?.formatter2Digits ?? "0.0") rms")
                    .font(.system(size:16))
                
                Text("\(record.properties?.mag?.formatter2Digits ?? "0.0") mag")
                    .font(.system(size:16))
            }
            .foregroundStyle(Color.contentTextColorApp)
            
            LocationLabelView(place: record.properties?.place ?? "")
            .frame(maxWidth: .infinity, alignment: .leading)
            .foregroundStyle(Color.accentColorApp.opacity(0.8))
            
        }
        .padding()
        .frame(maxWidth: .infinity)
        .background(Color.primaryColorApp.opacity(0.1))
        .clipShape(RoundedRectangle(cornerRadius: 16))
    }
}

#Preview {
    RecordRowView(record: DeveloperPreview.instance.record1)
}
