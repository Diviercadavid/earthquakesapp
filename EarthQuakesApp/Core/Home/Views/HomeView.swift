//
//  HomeView.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import SwiftUI

struct HomeView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject private var loginViewModel: LoginViewModel

    @StateObject private var homeViewModel: HomeViewModel = .init()
    
    @State private  var calendarId: UUID = UUID()
    @State private var startDate: Date = Date().yesterday
    @State private var endDate: Date = Date.now
    
    @State private var showDetail: Bool = false
    @State private var itemSelected: RecordModel?
    
    @StateObject var navManager: NavigationManager

    var body: some View {
        VStack {
            List {
                
                datesFilter
                
                listOfItems
                
                if homeViewModel.isLoading {
                    loadingView
                }
                
            }
            .accessibilityIdentifier("listItems")
            .navigationTitle("EarthQuakes App")
            .listStyle(.inset)
            .toolbar(content: {
                ToolbarItem(placement: .topBarTrailing) {
                    logoutButton
                }
                ToolbarItem(placement: .topBarLeading) {
                    aboutButton
                    
                }
            })
        }
        .alert(homeViewModel.showErrorAlertMessage, isPresented: $homeViewModel.showErrorAlert, actions: {
            Button("Close") {
                homeViewModel.showErrorAlert.toggle()
            }
        })
        .navigationBarBackButtonHidden()
        
    }
    
}

extension HomeView {
    
    private var datesFilter: some View {
        
        VStack {
            HStack() {
                Image(systemName: "calendar.badge.plus")
                Text("Start Date")
                DatePicker("", selection: $startDate, displayedComponents: [.date])
                    .accentColor(.pink)
                    .id(calendarId)
                    .onChange(of: startDate) {
                        calendarId = UUID()
                        homeViewModel.searchRecords(startDate: startDate, endDate: endDate)
                    }
                    .colorInvert()
                    .colorMultiply(.white)
                    .clipShape(RoundedRectangle(cornerRadius: 8))
            }
            .frame(maxWidth: .infinity)
            .frame(height: 50)
            .foregroundColor(.white)
            
            Color.white
                .frame(height: 1)
                .opacity(0.9)
            
            HStack {
                
                Image(systemName: "calendar.badge.plus")
                Text("End Date")
                DatePicker("", selection: $endDate, displayedComponents: [.date])
                    .id(calendarId)
                    .onChange(of: endDate) {
                        calendarId = UUID()
                        homeViewModel.searchRecords(startDate: startDate, endDate: endDate)
                    }
                    .colorInvert()
                    .colorMultiply(.white)
                    .clipShape(RoundedRectangle(cornerRadius: 8))
                
            }
            .foregroundColor(.white)
            .frame(maxWidth: .infinity)
            .frame(height: 50)
            
        }.foregroundStyle(Color.black.opacity(0.8))
            .padding()
            .background(Gradient(colors: [.primaryColorApp, .secondaryColorApp]))
            .clipShape(RoundedRectangle(cornerRadius: 6))
            
    }
    
    private var listOfItems: some View {
        ForEach(homeViewModel.recordsToShow) { item in
            
            VStack {
                RecordRowView(record: item)
                    .onTapGesture {
                        navManager.path.append(item)
                    }
                    .onAppear {
                        // Load next page when the last item is visible
                        if item.id == homeViewModel.recordsToShow.last?.id {
                            homeViewModel.loadNextPage()}
                    }
            }
        }
        
    }
    
    private var loadingView: some View {
        HStack {
            Text("Loading ")
            ProgressView()
        }.frame(maxWidth: .infinity)
    }
    
    private var logoutButton: some View {
        Button(action: {
            self.navManager.navigateToRoot()
            self.loginViewModel.logout()
        }, label: {
            HStack {
                Text("Logout")
                Image(systemName: "rectangle.portrait.and.arrow.right")
                    .font(.system(size: 12))
            }
            .foregroundColor(.accentColorApp.opacity(0.8))
            
        })
    }
    
    private var aboutButton: some View {
        Button {
            navManager.navigateTo(value: .about)
        } label: {
            Image(systemName: "info.circle")
                    .foregroundStyle(Color.accentColorApp.opacity(0.8))
        }
    }
    
}

#Preview {
    HomeView(navManager: NavigationManager())
}
