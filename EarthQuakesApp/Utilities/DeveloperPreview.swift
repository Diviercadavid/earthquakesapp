//
//  DeveloperPreview.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation

class DeveloperPreview {
    
    static let instance = DeveloperPreview()

    private init() { }
    
    //https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2020-01-01&endtime=2020-01-02
    let record1 = RecordModel(id: UUID().uuidString,
                              properties: Properties(title: "M 2.0 - 4 km S of Guánica, Puerto Rico",
                                                                 place: "4 km S of Guánica, Puerto Rico",
                                                                 mag: 2.03,
                                                                 depth: 0.06),
                              geometry: Geometry(coordinates: [-66.9036, 17.9343]))
    
    let record2 = RecordModel(id: UUID().uuidString,
                              properties: Properties(title: "M 1.1 - 70 km S of Shungnak, Alaska",
                                                     place: "70 km S of Shungnak, Alaska",
                                                     mag: 1.1,
                                                     depth: 0.5),
                              geometry: Geometry(coordinates: [-157.1638, 66.2601]))
    
    let record3 = RecordModel(id: UUID().uuidString,
                              properties: Properties(title: "M 1.1 - 3km SE of Lake Henshaw, CA",
                                                     place: "3km SE of Lake Henshaw, CA",
                                                     mag: 1.1,
                                                     depth: 0.19),
                              geometry: Geometry(coordinates: [-116.7383333, 33.2241667]))
    
    var responseObject: ResponseObtect {
        get {
            return ResponseObtect(features: [record1, record2, record3])
        }
    }
    
}

