//
//  NetworkingManager.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation
import Combine

enum CustomError: LocalizedError {
    case badURLResponse(url: URL, data: Data)
    case coreData(description: String)
    case unknown
    
    var errorDescription: String? {
        switch self {
        case .badURLResponse(url: let url, data: let data):
            return "[🔥] Bad response from URL: \(url). \n \(String(data: data, encoding: String.Encoding.utf8) ?? "")"
        case .coreData(let description):
            return "\(description)"
        case .unknown:
            return "<⚠️> Unknown error occurred"
        }
    }
}

class NetworkingManager {
    static func download(url: URL) -> AnyPublisher<Data, any Error> {
        return  URLSession.shared.dataTaskPublisher(for: url)
            .tryMap( {try handlerURLResponse(output: $0, url: url)} )
            .eraseToAnyPublisher()
    }
    
    static func handlerURLResponse(output: URLSession.DataTaskPublisher.Output, url: URL) throws -> Data {
        guard let response = output.response as? HTTPURLResponse,
              response.statusCode >= 200 && response.statusCode < 300 else {
            throw CustomError.badURLResponse(url: url, data: output.data)
        }
        return output.data
        
    }
    
    static func handleCompletionError<T: Error>(_ error: T, onError: @escaping (Error) -> Void) {
        // Perform any logging or analytics here if needed.
        print("logging or analytics error \(error.localizedDescription)")
        onError(error)
    }
}
