//
//  NavigationManager.swift
//  EarthQuakesApp
//
//  Created by Divier on 19/11/2023.
//

import SwiftUI

enum NavigationType: Hashable {
    case home
    case about
}

// viewModel Manager of Navigation Path
class NavigationManager: ObservableObject {
    @Published var path = NavigationPath()
    
    func navigateWithTag(tag: String) {
        path.append(tag)
    }
    
    func navigateBack() {
        path.removeLast()
    }
    
    func navigateToRoot() {
        path = NavigationPath()
    }
    
    func navigateTo(value: NavigationType){
        path.append(value)
    }
}
