//
//  RecordModel.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation

struct ResponseObtect: Codable {
    let features: [RecordModel]?
}

struct TestObject: Hashable {
    var name: String
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}

// MARK: - Feature
struct RecordModel: Identifiable, Codable, Hashable {
    
    
    var id: String?
    let properties: Properties?
    let geometry: Geometry?
    
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: RecordModel, rhs: RecordModel) -> Bool {
        return lhs.id == rhs.id && lhs.id == rhs.id
    }
}

// MARK: - Geometry
struct Geometry: Codable {
    let coordinates: [Double]?
}

// MARK: - Properties
struct Properties: Codable {
    let title: String?
    let place: String?
    let mag: Double?
    let depth: Double?
    
    enum CodingKeys: String, CodingKey {
        case title, place, mag
        case depth = "rms"
    }
    
}
