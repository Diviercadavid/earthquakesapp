//
//  earthquakesDataService.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation
import Combine

class EarthquakesDataService {
    
    @Published var error: Error? = nil
    @Published var recordsReturned:[RecordModel]?
    
    private var url: String = "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&"
    
    private var cancelableSubscription: AnyCancellable?
    
    init(){}
    
    deinit {
        cancelableSubscription = nil
    }
    
    func getRecordsByDates(startTime: String, endTime: String) {
        
        guard let url = URL(string: "\(url)starttime=\(startTime)&endtime=\(endTime)") else { return }
        print(url)
        
        cancelableSubscription = NetworkingManager.download(url: url)
            .decode(type: ResponseObtect.self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: handlerError(completion:), receiveValue: { [weak self] recordsReturned in
                self?.recordsReturned = recordsReturned.features
            })
    }
}

extension EarthquakesDataService {
    private func handlerError(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            NetworkingManager.handleCompletionError(error) { [weak self] handledError in
                guard let self = self else { return }
                self.error = handledError as Error
            }
        }
    }
  
}
