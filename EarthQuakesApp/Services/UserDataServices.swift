//
//  UserDataServices.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation
import CoreData

class UserDataServices {
    
    private let container: NSPersistentContainer
    private let containerName = "UserContainer"
    private let entityName = "UserEntity"
    
    @Published var userEntity: UserEntity = UserEntity()
    @Published var transactionResult: Result<String,CustomError>?
    
    static var shared = UserDataServices()
    
    private init() {
        container = NSPersistentContainer(name: containerName)
        container.loadPersistentStores { (_, error) in
            if let error = error {
                print("Error leading Core Data\(error)")
            }
        }
        createAdminUser()
    }
    
    
    
    func saveUser(name: String, surname: String, email: String, password: String) {
        
        guard searchUser(email: email) == nil else { 
            transactionResult = .failure(CustomError.coreData(description: "Error Saving data"))
            return }
        
        let userEntity = UserEntity(context: container.viewContext)
        userEntity.id = UUID()
        userEntity.name = name
        userEntity.surname = surname
        userEntity.email = email
        userEntity.password = password
        
        commitTransaction()
    }
    
    func getUser(email: String, password:String)  {
        validateUser(email, password)
    }
    
}

extension UserDataServices {
    
    private func validateUser(_ email: String,_ password: String) {
        
        let userFound = searchUser(email: email)
        
        if let userFound = userFound, isCorrectPassword(user: userFound, password: password) {
            userEntity = userFound
        } else {
            print("Error Login")
        }
    }
    
    
    private func searchUser(email: String) -> UserEntity? {
        let request = NSFetchRequest<UserEntity>(entityName: entityName)
        var userFound: UserEntity?
        
        do {
            try userFound = container.viewContext.fetch(request).first(where: {$0.email == email})
        } catch {
            print("Error getting fetching portfolio entities. \(error)")
        }
        return userFound
    }
    
    
    private func isCorrectPassword(user: UserEntity, password: String) -> Bool {
        return (user.password ?? "") == password
    }
    
    private func save() {
        do {
            try container.viewContext.save()
            transactionResult = .success("user saved successfully")
        } catch  {
            transactionResult = .failure(CustomError.coreData(description: "Error Saving data"))
        }
    }
    
    private func commitTransaction() {
        save()
        //get User Info
    }
    
    // test propose
    private func createAdminUser() {
        saveUser(name: "user", surname: "user", email: "Usr@gm.com", password: "Usr")
    }
}
