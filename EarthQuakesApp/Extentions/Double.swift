//
//  Double.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation
extension Double {
    var formatter2Digits: String {
        get { String(format: "%.2f", self) }
    }
}
