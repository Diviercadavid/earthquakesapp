//
//  Color.swift
//  EarthQuakesApp
//
//  Created by Divier on 17/11/2023.
//

import Foundation
import SwiftUI

extension Color {
    
    static let titleTextApp = Color("TitleTextColor")
    static let primaryColorApp = Color("PrimaryColor")
    static let secondaryColorApp = Color("SecondColor")
    static let accentColorApp = Color("AccentColor 1")
    static let contentTextColorApp = Color("ContentTextColor")
    
}
