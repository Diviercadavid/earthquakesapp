//
//  Date.swift
//  EarthQuakesApp
//
//  Created by Divier on 14/11/2023.
//

import Foundation
extension Date {
    
    var yesterday: Date { Calendar.current.date(byAdding: .day, value: -1, to: Date()) ?? Date.now }
    
    func simpleFormatterDate () -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        let formattedDate = dateFormatter.string(from: self)

        return formattedDate
    }
    
    
}
